Carbon App Manager
============================================

<a href='https://opensource.org/licenses/Apache-2.0'><img src='https://img.shields.io/badge/License-Apache%202.0-blue.svg'></a><br/>

[![pipeline status](https://gitlab.com/entgra/carbon-appmgt/badges/master/pipeline.svg)](https://gitlab.com/entgra/carbon-appmgt/commits/master)

Carbon App Manager (AppM) is a powerful platform for creating, managing, consuming and monitoring web Applications. It combines tried and tested SOA best practices
with modern day Application provisioning, management principles, governing and security
to solve a wide range of enterprise challenges associated with managing many number of
applications (Mobile and Web).
